
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <!--[if gt IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <![endif]-->
    <title>Dubai Holding Ramadan Greetings 2016 - Admin Panel</title>
    <link rel="icon" type="image/ico" href="img/favicon.ico"/>

    <link href="css/stylesheets.css" rel="stylesheet" type="text/css"/>
    <!--[if lte IE 7]>
    <link href="css/ie.css" rel="stylesheet" type="text/css"/>
    <script type='text/javascript' src='js/plugins/other/lte-ie7.js'></script>
    <![endif]-->
    <script type='text/javascript' src='js/plugins/jquery/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-ui-1.10.3.custom.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='js/plugins/other/excanvas.js'></script>

    <script type='text/javascript' src='js/plugins/other/jquery.mousewheel.min.js'></script>

    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>

    <script type='text/javascript' src='js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>

    <script type='text/javascript' src="js/plugins/uniform/jquery.uniform.min.js"></script>

    <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.js"></script>-->

    <script type='text/javascript' src='js/plugins/shbrush/XRegExp.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shCore.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushXml.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushJScript.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushCss.js'></script>

    <script type='text/javascript' src='js/plugins.js'></script>
    <script type='text/javascript' src='js/charts.js'></script>
    <script type='text/javascript' src='js/actions.js'></script>

</head>
<body>
<?php
session_start();

if(isset($_SESSION["islogged"])&&$_SESSION["islogged"]){?>


<div id="loader"><img src="img/loader.gif"/></div>
<div class="wrapper">


    <div class="body">

        <div class="container">

            <div class="page-headr">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="img/logo1.png" class="img-responsive"/>
                    </div>
                    <!--<div class="icon">
                        <span class="ico-layout-7"></span>
                    </div>-->
                    <div class="col-sm-8 header-h2">
                    <h1 class="text-center">Dubai Holding Ramadan Greetings 2016
                        <small class="text-center">Geerting List</small>
                    </h1>
                    </div>
                    <div class="col-sm-2">
                        <img src="img/logo2.png" class="img-responsive pull-right"/>
                    </div>
                </div>


                <div class="clear"></div>

            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a  class="btn btn-primary text-right" href="#" id="test" onClick="javascript:fnExcelReport();">Export</a>
                    <a class="btn btn-danger text-right" href="logout.php">Logout</a>


                </div>

                <div class="col-sm-3 offset10">
                    </div>
            </div>

            <div class="row">
                <div class="col-md-12">


                    <div class="block">
                        <!--<div class="head dblue">
                            <div class="icon"><span class="ico-layout-9"></span></div>
                            <h2>Table sorting pagination</h2>
                            <ul class="buttons">
                                <li><a href="#" onClick="source('table_sort_pagination');
                                            return false;">
                                        <div class="icon"><span class="ico-info"></span></div>
                                    </a></li>
                            </ul>
                        </div>-->
                        <div class="data-fluid">
                            <table class="table fpTable lcnp" cellpadding="0" cellspacing="0" width="100%" id="myTable">
                                <thead>
                                <tr>
                                    <th width="20%">Name</th>
                                    <th>Message</th>
                                    <th width="20%">Submission Date</th>

                                </tr>
                                </thead>
                                <tbody>

                                    <?php

                                    $servername = "localhost";
                                    $username = "root";
                                    $password = "";
                                    $dbname = "db_dubaihoding";

                                    // Create connection

                                    $conn = new mysqli($servername, $username, $password, $dbname);

                                    // Check connection
                                    if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                    }

                                    $sql = "SELECT id, user_name, message, submission_date FROM messages";

                                    //mysqli_set_charset ( $conn , "SET NAMES 'utf8'" );
                                    $conn->set_charset("utf8");
                                    $result = $conn->query($sql);

                                    /*var_dump($result);*/

                                    if ($result->num_rows > 0) {

                                        while($row = $result->fetch_assoc()) {
                                            echo "<tr>";
                                            echo "<td class='ar'>". $row["user_name"]. "</td>";
                                            echo "<td class='ar'>" . $row["message"] . "</td>";
                                            echo "<td>" . $row["submission_date"] . "</td>";
                                            echo "</tr>";
                                        }
                                        echo "</table>";
                                    }

                                    $conn->close();
                                    ?>

                                    <!--<td><a href="#">Dmitry Ivaniuk</a></td>
                                    <td>Product #212</td>
                                    <td><span class="label label-important">New</span></td>-->


                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>
<div class="dialog" id="source" style="display: none;" title="Source"></div>

<?php } else{
    header("Location:login.php");
}?>
</body>
</html>

<script>
    function fnExcelReport() {
        var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
        tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

        tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';

        tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
        tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

        tab_text = tab_text + "<table border='1px'>";
        tab_text = tab_text + $('#myTable').html();
        tab_text = tab_text + '</table></body></html>';

        var data_type = 'data:application/vnd.ms-excel';

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            if (window.navigator.msSaveBlob) {
                var blob = new Blob([tab_text], {
                    type: "application/csv;charset=utf-8;"
                });
                navigator.msSaveBlob(blob, 'Test file.xls');
            }
        } else {
            $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
            $('#test').attr('download', 'Dubai Holding Ramadan Greetings 2016 Report.xls');
        }

    }
</script>
