-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2016 at 02:46 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dubaihoding`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `submission_date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_name`, `message`, `submission_date`) VALUES
(64, 'حمدان', 'ايمان', '2016-06-02 08:12:48'),
(68, 'Rana', 'Peace', '2016-06-02 13:03:06'),
(71, 'لتنن', 'اتمنا', '2016-06-03 05:57:38'),
(169, 'Essa Alzaabi', 'giving', '2016-06-08 04:38:12'),
(168, 'mercy', 'saleem', '2016-06-08 03:52:14'),
(83, 'الرفاتي', 'رحمه', '2016-06-05 02:00:33'),
(84, 'Gunjan', 'Family', '2016-06-05 03:34:42'),
(85, 'منى عناية', 'صفاءالروح', '2016-06-05 06:26:24'),
(167, 'سالم', 'اطمئنان', '2016-06-08 03:49:16'),
(87, 'Vidya Menon', 'Holy', '2016-06-05 07:49:03'),
(88, 'Wessam Ibrahim', 'Gathering', '2016-06-05 07:49:25'),
(89, 'RONY PRAVEEN', 'Caring', '2016-06-05 07:49:32'),
(90, 'Nabilah Al Jaidi', 'الترابط', '2016-06-05 07:49:35'),
(91, 'Yajant', 'Empathy', '2016-06-05 07:49:53'),
(92, 'mohammad abu haltam', 'mercy', '2016-06-05 07:50:22'),
(93, 'Ashwini ', 'Reflections ', '2016-06-05 07:50:46'),
(94, 'Udayakumar Kannappan', 'Blessings ', '2016-06-05 07:52:31'),
(95, 'jamila yusuf', 'iman', '2016-06-05 07:52:56'),
(96, 'Zeeshan Ali Khan', 'Beholden', '2016-06-05 07:53:06'),
(97, 'Pubudu Perera', 'Sharing', '2016-06-05 07:53:13'),
(98, 'Gulam Dasthageer ', 'Guidance ', '2016-06-05 07:53:45'),
(99, 'احمد غنيم', 'تقوي', '2016-06-05 07:54:00'),
(100, 'khalifa', 'redemption ', '2016-06-05 07:54:02'),
(101, 'Ameneh kashnan ', 'الاجر', '2016-06-05 07:54:04'),
(102, 'Aleks', 'Virtue', '2016-06-05 07:54:46'),
(103, 'Farid Mohammad Saadi ', ' Humble ', '2016-06-05 07:54:57'),
(104, 'Abdul Wajid', 'Taqwa', '2016-06-05 07:56:00'),
(105, 'mohamed hussain', 'blessings', '2016-06-05 07:56:11'),
(106, 'Quadir', 'sacredness', '2016-06-05 07:57:20'),
(107, 'Khurram Shahzad', 'Blessings', '2016-06-05 07:59:45'),
(108, 'jameel.moidheen@dpg.ae', 'peace', '2016-06-05 08:00:20'),
(109, 'Libu Mathew', 'Divine ', '2016-06-05 08:01:36'),
(110, 'Salwa Bayaty', 'Blessings', '2016-06-05 08:01:55'),
(111, 'M.A HAFEEZ KHAN', 'purify', '2016-06-05 08:02:09'),
(112, 'Mohammed Kaleemullah', 'Awesome', '2016-06-05 08:02:23'),
(113, 'MOHAMED AHMED MOHSEN', 'الرحمه', '2016-06-05 08:11:32'),
(114, 'Rajeesh Majeed', 'sharing', '2016-06-05 08:12:16'),
(115, 'Salim Shaikh', 'Blessing', '2016-06-05 08:14:18'),
(116, 'Arif karam', 'الرحمة', '2016-06-05 08:19:12'),
(117, 'Anu Thomas', 'Helping', '2016-06-05 08:21:29'),
(118, 'Thrity Thanavala', 'Charity', '2016-06-05 08:34:25'),
(119, 'VICTOR', 'NO', '2016-06-05 08:43:16'),
(120, 'Sumantra Ghosh', 'Respect', '2016-06-05 09:02:02'),
(121, 'Preea V Mane', 'Charity', '2016-06-05 09:19:04'),
(122, 'Karbhari Roshan Khan', 'Ibadat', '2016-06-05 09:32:17'),
(123, 'Bilal Siddiqui', 'Patience', '2016-06-05 09:51:43'),
(124, 'Syed Bazmi Ali', 'Forgiveness', '2016-06-05 11:18:45'),
(125, 'mohsin bin yahiya BinMahfooz', 'forgivness', '2016-06-05 12:47:55'),
(126, 'Eissa Barhoum', 'البركة', '2016-06-05 13:05:33'),
(127, 'Akram Yakout ', 'Spirit', '2016-06-05 13:17:08'),
(128, 'Majid Saleem', 'Humbleness', '2016-06-05 14:32:42'),
(129, 'مشعل عبدالله', 'قربان', '2016-06-05 14:40:24'),
(130, 'رائد عبدالرحيم الاحمد ', 'روحانية', '2016-06-05 15:41:42'),
(131, 'Rahima', 'Family ', '2016-06-05 22:57:05'),
(132, 'أنس', 'التوبة', '2016-06-05 23:00:37'),
(133, 'Manzoor Ahmed', 'CARE', '2016-06-05 23:37:38'),
(134, 'Mona Amin', 'StartingOver', '2016-06-05 23:39:08'),
(135, 'احمد سمير', 'السلام', '2016-06-05 23:39:43'),
(136, 'Manal Mahfouz', 'forgiveness', '2016-06-05 23:45:12'),
(137, 'Rashmi Roshan', 'Piousness', '2016-06-06 00:03:17'),
(138, 'عبد الرزاق ميتال', 'تقوى', '2016-06-06 00:06:13'),
(139, 'FORGIVENESS', 'BLESSING', '2016-06-06 00:09:53'),
(140, 'Hussam Younis', 'forgiveness', '2016-06-06 00:22:59'),
(141, 'عبدالهادي زقزوق', 'الصبر', '2016-06-06 00:31:45'),
(142, 'Khalil Ksaar ', 'purity ', '2016-06-06 01:04:35'),
(143, 'غادة الخلصان', 'Love', '2016-06-06 01:07:21'),
(144, 'dsfsd sdfsdf', 'dssdsd', '2016-06-06 01:12:20'),
(145, 'Reghu Mettamel', 'Caring', '2016-06-06 01:12:26'),
(146, 'Huda', 'Peace', '2016-06-06 01:14:19'),
(147, 'Kathy', 'sacrificing', '2016-06-06 01:23:56'),
(148, 'سامح حسن', 'فرحة', '2016-06-06 01:37:07'),
(149, 'Muhammed Sajjad Kalliyadan Poil', ' refocusongod', '2016-06-06 01:42:46'),
(150, 'ياسر جابر', 'التسامح', '2016-06-06 02:12:29'),
(151, 'Majed Almansoori', 'Tolerance', '2016-06-06 02:31:31'),
(152, 'محمد لوتاه', 'الحب', '2016-06-06 04:22:59'),
(153, 'Sarah Shaw', 'Patience', '2016-06-06 04:54:19'),
(154, 'Adonis Victoria', 'contentment', '2016-06-06 05:19:29'),
(155, 'abhiram', 'bliss', '2016-06-06 05:24:47'),
(156, 'Fahad Alkhaja', 'mildness', '2016-06-06 06:08:24'),
(157, 'علاء مغاري', 'التعاطف', '2016-06-07 00:45:59'),
(158, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:00'),
(159, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:00'),
(160, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:00'),
(161, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:02'),
(162, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:09'),
(163, 'علاء مغاري', 'التعاطف', '2016-06-07 00:46:09'),
(164, 'فاطمة الفارسي', 'المغفره ', '2016-06-07 00:49:00'),
(165, 'حمده محمد أهلي', 'عباده', '2016-06-07 04:17:40'),
(166, 'Emad AboElkher', 'Integration ', '2016-06-08 02:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`) VALUES
(1, 'admin', 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
